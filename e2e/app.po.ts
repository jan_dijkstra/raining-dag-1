export class HuisBibliotheekPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('huis-bibliotheek h1')).getText();
  }
}
