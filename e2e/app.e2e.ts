import { HuisBibliotheekPage } from './app.po';

describe('angular2app App', function() {
  let page: HuisBibliotheekPage;

  beforeEach(() => {
    page = new HuisBibliotheekPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('angular2app works!');
  });
});
