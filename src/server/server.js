console.log('loading libraries');
var express = require('express');
var cors = require('cors');
var app = express();
var http = require('http').Server(app);
var bodyParser = require('body-parser');

(function StockService() {
  initHttpServer();
})();

function initHttpServer() {
  console.log('starting http on 3004');
  app.use(cors());
  app.use(bodyParser.json());

  app.listen('3004', function () {
    console.log('started http on 3004');

  });

  app.get('/overzicht', handleOverzicht);
  function handleOverzicht(request, response) {
    var top5 = overzicht.concat([]);
    top5.forEach(function (entry) {

      entry.leenHistorie = entry.leenHistorie
        .sort(function (a, b) {
          return a.datum < b.datum ? 1 : a.datum > b.datum ? -1 : 0
        });

      if (entry.leenHistorie.length > 5) {
        entry.leenHistorie = entry.leenHistorie.slice(0, 5);
      }
    });

    response.json(top5);
  }

  app.get('/detail/:id', handleDetail);
  function handleDetail(request, response) {
    var detail = overzicht.find(function (entry) {
      return entry.id == request.params.id
    });

    detail && (detail.leenHistorie = detail.leenHistorie.sort(function (a, b) {
      return a.datum < b.datum ? 1 : a.datum > b.datum ? -1 : 0
    }));

    response.json(detail);
  }

  app.post('/store', handleStore);
  function handleStore(request, response) {
    console.log('store reached');
    if (request.body.titel && request.body.auteur) {
      overzicht.push({
        id: "10000" + (overzicht.length + 1),
        titel: request.body.titel,
        auteur: request.body.auteur,
        img: "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg",
        leenHistorie: []
      });
      console.log(`store pushed ${request.body.titel}`);

      response.json({success: true})

    } else {
      response.json({success: false, request: request.body})

    }
  }
}


var overzicht = [
  {
    "id": 100001,
    "titel": "Frieda Klein 6 - Als het zaterdag wordt",
    "auteur": "Nicci French",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/4/7/5/9/9200000051759574.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Jan"
      },
      {
        "datum": 1453057608644,
        "naam": "Deborah"
      }
    ]
  },
  {
    "id": 100002,
    "titel": "Hittegolf",
    "auteur": "Suzanne Vermeer",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/9/7/6/4/9200000051724679.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Kees"
      },
      {
        "datum": 1453057608644,
        "naam": "Francien"
      },
      {
        "datum": 1453057607644,
        "naam": "Jan"
      }
    ]
  },
  {
    "id": 100003,
    "titel": "Amos Decker - De laatste mijl",
    "auteur": "David Baldacci",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/1/6/6/4/9200000051724661.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Jan"
      },
      {
        "datum": 1453057608644,
        "naam": "Hein"
      },
      {
        "datum": 1453057607644,
        "naam": "Alexandra"
      },
      {
        "datum": 1453057606644,
        "naam": "Corrie"
      }
    ]
  },
  {
    "id": 100004,
    "titel": "Maestra",
    "auteur": "L.S. Hilton",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/1/2/0/1/9200000053531021.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "John"
      },
      {
        "datum": 1453057608644,
        "naam": "Sander"
      },
      {
        "datum": 1453057607644,
        "naam": "Marieke"
      }
    ]
  },
  {
    "id": 100005,
    "titel": "Verborgen",
    "auteur": "Karin Slaughter",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/4/4/9/7/9200000057137944.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Suzanne"
      },
      {
        "datum": 1453057608644,
        "naam": "Jan"
      },
      {
        "datum": 1453057607644,
        "naam": "Deborah"
      }
    ]
  },
  {
    "id": 100006,
    "titel": "Zeezicht",
    "auteur": "Linda van Rijn",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/6/0/9/8/9200000046418906.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Alexandra"
      },
      {
        "datum": 1453057608644,
        "naam": "Jan"
      },
      {
        "datum": 1453057607644,
        "naam": "Marieke"
      },
      {
        "datum": 1453057606644,
        "naam": "Piet"
      }
    ]
  },
  {
    "id": 100007,
    "titel": "Sourcery",
    "auteur": "Terry Pratchett",
    "img": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/6/7/1/8/1001004000968176.jpg",
    "leenHistorie": [
      {
        "datum": 1453057609644,
        "naam": "Jan"
      },
      {
        "datum": 1453057608644,
        "naam": "Sander"
      },
      {
        "datum": 1453057607644,
        "naam": "Francien"
      },
      {
        "datum": 1453057606644,
        "naam": "Jan"
      }
    ]
  }
];
