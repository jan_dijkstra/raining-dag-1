# Inhoud

## TypeScript
* [Types](#typescript-types)
* [Getters en Setters](#getter-setter)
* [import syntax](#import-syntax)

## Component

* [Component](#component)

## Getter Setter
```javascript

    // myCmp.upperCaseTitel -> "HUIS BIBLIOTHEEK"
    // myCmp.upperCaseTitel = "Huis Bibliotheek" -> set de titel naar een uppercase waarde
    class MyCmp {
        get upperCaseTitel():string {
            return titel.toUpperCase();
        }
        
        set upperCaseTitel(titel:string) {
            this.titel = titel.toUpperCase();
        }
    }
```
    
## Typescript Types   
```javascript
                  
'any'       alle andere types erven over van any  
'number'    getallen, digits                      
'string'    text                                  
'boolean'   true, false                           
'symbol'    immutable                             
'void'      Void                                  
```

## Import syntax
Voor het importeren van de default class, mits Boek is gedefinieerd als `export default class Boek`
```javascript 
import Boek from '../models/Boek'
```

Voor het importeren van meerdere classes of interfaces uit 1 bestand.
```javascript 
import { Boek, BoekDetail } from '../models/Boek'
```

voor het importeren van alle classes, waarna de definities bereikbaar zijn met `notatieLib.Boek`
```javascript 
import * as notatieLib from '../models/Boek'
```    

## Component
`@Component` is een functie uit `@angular/core` en heeft als parameter een Object `{ }`. Dit object bevat de configuratie voor dit component.

De meest gebruikte configuratie.
```javascript
@Component({
    moduleId                    // string; maakt relatieve paden mogelijk, alleen te gebruiken in combinatie met CommonJS of SystemJS.
    selector                    // string; De naam van het component die gebrukt wordt in de template
    template | templateUrl      // string; De template zelf met back ticks``, of de url naar de template. Als moduleId is gezet, is een relatief pad voldoende
    providers                   // array; Hier worden de providers aangegeven die op dit niveau worden geinstantieerd. Child components krijgen dezelfde instantie.
    directives                  // array; Hier worden de directives / componenten aangegeven die in de template worden gebruikt
    pipes                       // array; Hier worden de popes aangegeven die in de template worden gebruikt
    styles | stylesUrl          // array | string; Hier kan component specifieke CSS worden ingeladen, of verwezen naar een bestand
})
```
